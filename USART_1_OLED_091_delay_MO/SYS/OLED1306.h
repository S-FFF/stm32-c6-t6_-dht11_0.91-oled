#ifndef __OLED1306_H
#define __OLED1306_H
#include "sys.h"

#define IIC_SCL PBout(6)//SCL
#define IIC_SDA PBout(7)//SDA
#define READ_SDA PBin(7)
#define OLED_IIC_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=(u32)3<<28;}
#define OLED_IIC_IN() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=(u32)8<<28;}

#define OLED_CMD 0
#define OLED_Data 1
#define OLED_Addr 0x78 //�ӻ���ַ



void OLED_I2C_Init(void);
void OLED_I2C_START(void);
void OLED_I2C_STOP(void);
u8 IIC_Wait_Ack(void);
void IIC_Ack(void);
void IIC_NAck(void);
u8 OLEDIIC_Check_Addr(u8 addr);
void OLED_Write(u8 data);
u8 OLED_Read(unsigned char ack);
void OLED_WriteData(u8 Data);
static void OLED_WriteCmd(u8 cmd);
void OLED_Clear(void);
void OLED_WR_Byte(unsigned dat,unsigned cmd);
void OLED_ON(void);
void OLED_Set_Pos(u8 x,u8 y);
void OLED_ShowChar(u8 x,u8 y,u8 size,u8 Char);
void OLED_ShowString(u8 x,u8 y,u8 Size,u8 *chr);
void OLED_ShowNum(u8 x,u8 y,u16 num,u8 len,u8 size);
void OLED_ShowChinese(u8 x,u8 y,u8 Numb);
#endif
