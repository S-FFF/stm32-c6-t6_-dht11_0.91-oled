#include "sys.h"  //包含需要的头文件


void OLED_I2C_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
		
	//I2C1_SCL PB6
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	//I2C1_SDA PB7
	
	IIC_SDA=1;
	IIC_SCL=1;
	
	
	delay_ms(200);
	OLED_WR_Byte(0xAE,OLED_CMD);//关闭显示
	
	OLED_WR_Byte(0x40,OLED_CMD);//---set low column address
	OLED_WR_Byte(0xB0,OLED_CMD);//---set high column address

	OLED_WR_Byte(0xC8,OLED_CMD);//-not offset

	OLED_WR_Byte(0x81,OLED_CMD);//设置对比度
	OLED_WR_Byte(0xff,OLED_CMD);

	OLED_WR_Byte(0xa1,OLED_CMD);//段重定向设置

	OLED_WR_Byte(0xa6,OLED_CMD);//
	
	OLED_WR_Byte(0xa8,OLED_CMD);//设置驱动路数
	OLED_WR_Byte(0x1f,OLED_CMD);
	
	OLED_WR_Byte(0xd3,OLED_CMD);
	OLED_WR_Byte(0x00,OLED_CMD);
	
	OLED_WR_Byte(0xd5,OLED_CMD);
	OLED_WR_Byte(0xf0,OLED_CMD);
	
	OLED_WR_Byte(0xd9,OLED_CMD);
	OLED_WR_Byte(0x22,OLED_CMD);
	
	OLED_WR_Byte(0xda,OLED_CMD);
	OLED_WR_Byte(0x02,OLED_CMD);
	
	OLED_WR_Byte(0xdb,OLED_CMD);
	OLED_WR_Byte(0x49,OLED_CMD);
	
	OLED_WR_Byte(0x8d,OLED_CMD);
	OLED_WR_Byte(0x14,OLED_CMD);
	
	OLED_WR_Byte(0xaf,OLED_CMD);
	OLED_Clear();
	
	
}

void OLED_I2C_START(void)
{
	OLED_IIC_OUT();
	IIC_SDA=1;
	IIC_SCL=1;
	delay_us(4);
	IIC_SDA=0;
	delay_us(4);
	IIC_SCL=0;
}
void OLED_I2C_STOP(void)
{
	OLED_IIC_OUT();
	IIC_SCL=0;
	IIC_SDA=0;
	delay_us(4);
	IIC_SCL=1;
	IIC_SDA=1;
	delay_us(4);
}






// Ack部分
u8 IIC_Wait_Ack(void)
{
	u8 ErrTime=0;
	OLED_IIC_IN();      //SDA设置为输入  
	IIC_SDA=1;
	delay_us(2);	   
	IIC_SCL=1;
	delay_us(2);	
	while(READ_SDA)
	{
		ErrTime++;
		if(ErrTime>250)
		{
			OLED_I2C_STOP();
			return 1;
		}
	}
	IIC_SCL=0;    //时钟输出0 	   
	return 0;  
}
void IIC_Ack(void)
{
	IIC_SCL=0;
	OLED_IIC_OUT();
	IIC_SDA=0;
	delay_us(2);
	IIC_SCL=1;
	delay_us(2);
	IIC_SCL=0;
}
//不产生ACK应答		    
void IIC_NAck(void)
{
	IIC_SCL=0;
	OLED_IIC_OUT();
	IIC_SDA=1;
	delay_us(2);
	IIC_SCL=1;
	delay_us(2);
	IIC_SCL=0;
}	





u8 OLEDIIC_Check_Addr(u8 addr)//检查从机是否有应答
{
	u8 ack;
	OLED_I2C_START();
	OLED_Write(addr);
	ack=IIC_Wait_Ack();
	return ack;
	
}



// 写数据写命令函数
void OLED_WriteData(u8 Data)
{
	OLED_I2C_START();
	OLED_Write(0x78);		//第一次发送从机地址
	IIC_Wait_Ack();
	OLED_Write(0x40);		//第二次发送数据地址
	IIC_Wait_Ack();
	OLED_Write(Data);
	IIC_Wait_Ack();
	OLED_I2C_STOP();
}
static void OLED_WriteCmd(u8 cmd)
{
	OLED_I2C_START();
	OLED_Write(0x78); //第一次发送从机地址
	IIC_Wait_Ack();
	OLED_Write(0x00); //第二次发送命令地址
	IIC_Wait_Ack();
	OLED_Write(cmd);  //写入指定命令
	IIC_Wait_Ack();
	OLED_I2C_STOP();
}











// 读写OLED函数
void OLED_Write(u8 data)
{
	OLED_IIC_OUT();//SDA设置为输出
	IIC_SCL=0;
	for(u8 mask=0x80;mask!=0;mask>>=1)
	{
		if((mask&data)==0)
		{
			IIC_SDA=0;
		}
		else IIC_SDA=1;	
		delay_us(4);
		IIC_SCL=1;
		delay_us(4);
		IIC_SCL=0;
		//delay_us(1);
		
	}
}

u8 OLED_Read(unsigned char ack)
{
	unsigned char i,receive=0;
	OLED_IIC_IN();        //SDA设置为输入
    for(i=0;i<8;i++ )
	{
        IIC_SCL=0; 
        delay_us(2);
		IIC_SCL=1;
        receive<<=1;
        if(READ_SDA)receive++;   
		delay_us(1); 
    }					 
    if (!ack)
        IIC_NAck();        //发送nACK
    else
        IIC_Ack();         //发送ACK   
    return receive;
}



void OLED_WR_Byte(unsigned dat,unsigned cmd)
{
	if(cmd)
	{
	OLED_WriteData(dat);
	}
	else 
	{
	OLED_WriteCmd(dat);
	}


}

void OLED_Clear(void)  
{  
	u8 i,n;		    
	for(i=0;i<8;i++)  
	{  
		OLED_WR_Byte (0xb0+i,OLED_CMD);    //设置页地址（0~7）
		OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址   
		for(n=0;n<128;n++)OLED_WR_Byte(0,OLED_Data); 
	} //更新显示
}

//设置光标
void OLED_Set_Pos(u8 x,u8 y) //x列 y页
{
	OLED_WR_Byte(0xb0+y,OLED_CMD); //设置页 0xB0代表页为第0页
	OLED_WR_Byte((x&0xf0)>>4|0x10,OLED_CMD); //设置列 高四位 获取高四位数据右移动4位并保存，且让第5位为1
	OLED_WR_Byte((x&0x0f),OLED_CMD); //设置列 低四位 保留最后一位数据
}
//开启显示
void OLED_ON(void)
{
	OLED_WR_Byte(0xAF,OLED_CMD); //开启显示
	OLED_WR_Byte(0x8D,OLED_CMD); //设置电荷泵
	OLED_WR_Byte(0x14,OLED_CMD); //开启电荷泵
}

//显示字符
void OLED_ShowChar(u8 x,u8 y,u8 size,u8 Char)//传入列，页，字符
{
	u8 c,i;
	
	c = Char - ' ';//减掉space空格的ASCII码，得到字符的偏移量
			
	if(x>128)
	{
		x = 0;
		y += 2;
	}
	if(size==16)//8*16的点阵
	{
	OLED_Set_Pos(x,y);
	
	for(i=0;i<8;i++)
	{
		OLED_WR_Byte(F8X16[c*16+i],OLED_Data);
	}
	
	OLED_Set_Pos(x,y+1);
	
	for(i=0;i<8;i++)
	{
	OLED_WR_Byte(F8X16[c*16+i+8],OLED_Data);
	}
	}
	else 
	{	
	OLED_Set_Pos(x,y);  //6*8的点阵
	for(i=0;i<6;i++)
	OLED_WR_Byte(F6x8[c][i],OLED_Data);	  //二维数组，c控制第几行，i控制第几列
	}
}


//显示字符串
void OLED_ShowString(u8 x,u8 y,u8 Size,u8 *chr)  //显示字符串
{
	u8 j=0;
	while (chr[j]!='\0')  //判断字符串是否结束
	{		
		OLED_ShowChar(x,y,Size,chr[j]); // 一个一个画字符
		x+=8;   //x 设置的是列，一个字符的大小为8*16，即行16列8，每次显示为一个后，都需要向高列移8列
		if(x>120){x=0;y+=2;} // 最高为128列，超过的话则需要重新从零列开始，由于此时需要别的页数来续画，避免重叠，需要 y += 2。
		j++; //循环画字符串
	}
}



unsigned int OLED_Pow(unsigned char m,unsigned char n)
{	//假设m=10，n=1 2 3 4 ...
	unsigned char resulut = 1;
	
	while(n--)       //n=1，n=2，n=3
	{
		resulut *= m; //   ，10 ，100
	}
	
	return resulut;   // 1 ，10 ，100
}
//显示数字
void OLED_ShowNum(u8 x,u8 y,u16 num,u8 len,u8 size)//123
{	//传入参数 x,y 列和页;  num：对应数字;  len：数字个数;  size：间隔大小
	
	if(size==8)size=12;//8*6
	u8 temp;
	u8 headflag = 0; //是否为第一个数是否为0的标志
	
	for(u8 t=0;t<len;t++)
	{
		temp = num/OLED_Pow(10,len-t-1)%10; //将数字一位一位地取出来
		if(headflag == 0 && t < (len-1)&&temp==0) //排除掉num = 0 的时候
		{
			OLED_ShowChar(x+(size/2)*t,y,size,' '); //如果数字开头为0，用space空格字符来表示
			continue; //跳出循环，避免重复显示
		}
		else
		{
			headflag = 1;
		}
		
		OLED_ShowChar(x+(size/2)*t,y,size,temp+'0'); //temp+'0'：'0' 从0开始的偏移量
	}
}
//显示文字
void OLED_ShowChinese(u8 x,u8 y,u8 Numb)
{
	u8 i = 0;
	
	OLED_Set_Pos(x,y);
	
	for(i=0;i<16;i++) //写汉字的上一半
	{
		OLED_WR_Byte(F16X16[Numb*2][i],OLED_Data); //NO*2：距离字库中第一个汉字的偏移量，i：第i个数据
	}
	
	OLED_Set_Pos(x,y+1);
	for(i=0;i<16;i++) //写汉字的下一半
	{
		OLED_WR_Byte(F16X16[Numb*2+1][i],OLED_Data); //NO*2+1：距离字库中第一个汉字的偏移量 +1：汉字下一半的数组，i：第i个数据
	}
}




